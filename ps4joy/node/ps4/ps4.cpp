#include <ros/ros.h>
#include <autoware_msgs/PS4Joy.h>
#include <sensor_msgs/Joy.h>

autoware_msgs::PS4Joy cmd_vel;
void joy_callback(const sensor_msgs::Joy& joy_msg)
{
  cmd_vel.twist.linear.x = joy_msg.axes[1];
  cmd_vel.twist.linear.y = 0;
  cmd_vel.twist.angular.z = joy_msg.axes[0];
  cmd_vel.main_button.clear();
  for(int i=0;i<4;i++) cmd_vel.main_button.push_back(joy_msg.buttons[i]);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "ps4joy_twist_publisher");
  ros::NodeHandle nh;
  ros::Publisher cmd_pub = nh.advertise<autoware_msgs::PS4Joy>("/turtle1/cmd_vel", 10);
  ros::Subscriber joy_sub = nh.subscribe("joy", 10, joy_callback);

  ros::Rate loop_rate(10);
  while (ros::ok())
  {
    cmd_pub.publish(cmd_vel);
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}

