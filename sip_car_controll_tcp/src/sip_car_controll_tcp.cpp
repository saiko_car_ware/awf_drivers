#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <autoware_msgs/SipSignalInfo.h>

/*struct SignalFirstInfo
{
	unsigned char prefectures_code_;//都道府県コード
	unsigned char offer_point_code_;//提供点種別コード
	unsigned short intersection_id_;//交差点ID
	unsigned char specific_control_flag_;//特定制御フラグ
	unsigned char system_state_;//システム状態
	unsigned char event_counter_;//イベントカウンター
	unsigned char car_lamp_count_;//車灯器数
	unsigned char walk_lamp_count_;//歩灯器数
	unsigned char connection_routes_count_;//接続方路数
	unsigned char service_routes_count_;//サービス方路数
};

struct ServiceInfo
{
	unsigned char routes_id_;//方路ID
	unsigned char direction_existence_flag_;//信号通行方向情報有無フラグ
	char direction_info_;//信号通行方向情報
	std::vector<short> car_lamp_info_ptr_;//車灯器情報のポインタ一覧
	std::vector<short> walk_lamp_info_ptr_;//歩灯器情報のポインタ一覧
};

struct CarLampInfo
{
	unsigned char round_signal_color_view_;//丸信号灯色表示
	unsigned char blue_arrow_signal_color_view_;//青矢印信号灯色表示
	unsigned char countdown_stop_flag_;//カウントダウン停止フラグ
	unsigned short min_remain_sec_;//最小残秒数
	unsigned short max_remain_sec_;//最大残秒数
};

struct CarLampAllInfo
{
	unsigned int addres_number_;//データ上の参照アドレス
	unsigned char car_lamp_id_;//車灯器ID
	//unsigned char color_change_count_;//灯色出力変化数
	std::vector<CarLampInfo> car_lamp_info_;//車灯器情報一覧
};*/

class SipCarControll
{
private:
	ros::NodeHandle nh_, p_nh_;
	ros::Publisher pub_signal_info_, pub_tmp_, pub_alive_;

	int sock_;
	int read_address_;

	short short_read(unsigned char* buf)
	{
		short short_data;
		unsigned char* byte_data = (unsigned char*)&short_data;
		byte_data[1] = buf[read_address_];
		byte_data[0] = buf[read_address_+1];
		read_address_ += 2;
		return short_data;
	}

	unsigned short ushort_read(unsigned char* buf)
	{
		unsigned short short_data;
		unsigned char* byte_data = (unsigned char*)&short_data;
		byte_data[1] = buf[read_address_];
		byte_data[0] = buf[read_address_+1];
		read_address_ += 2;
		return short_data;
	}

	char byte_read(char* buf)
	{
		char data = buf[read_address_];
		read_address_ += 1;
		return data;
	}

	unsigned char ubyte_read(unsigned char* buf)
	{
		unsigned char data = buf[read_address_];
		read_address_ += 1;
		return data;
	}
public:
	SipCarControll(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
	{
		std::string ip_str;
		p_nh.getParam("ip", ip_str);
		std::cout << ip_str << std::endl;
		int sock = socket(PF_INET, SOCK_STREAM, 0);//ソケットの作成

		//接続先指定用構造体の準備
		struct sockaddr_in server;
		server.sin_family = PF_INET;
		server.sin_port = htons(52000);
		server.sin_addr.s_addr = inet_addr(ip_str.c_str());

		//サーバに接続
		if(connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
			std::cout << "error" << std::endl;
		sock_ = sock;

		pub_signal_info_ = nh_.advertise<autoware_msgs::SipSignalInfo>("/sip/signal_info", 10);
		pub_alive_ = nh_.advertise<std_msgs::String>("/sip/signal_info", 10);
	}

	~SipCarControll()
	{
		close(sock_);
	}

	void run()
	{
		std_msgs::String str_alive;
		str_alive.data = "/sip_car_controll_tcp";
		pub_alive_.publish(str_alive);

		const int BUFSIZE = 1000;
		unsigned char buf[BUFSIZE];
		int len=read(sock_, buf, sizeof(buf));
		//std::cout << len << std::endl;
		//for(int i=0;i<len;i++) printf("%d,", buf[i]);
		//printf("\n"); fflush(stdout);

		read_address_ = 0;

		unsigned char data_type = ubyte_read(buf);
		std::cout << +data_type << std::endl;
		switch(data_type)
		{
		case 0x00://信号情報
			{
				unsigned short data_size = ushort_read(buf);
				//std::cout << data_size << std::endl;

				autoware_msgs::SipSignalInfo ret;

				//SignalFirstInfo signal_first_info;
				autoware_msgs::SipSignalFirstInfo signal_first_info;
				unsigned char ubyte_data;
				unsigned short ushort_data;

				read_address_ += 9;
				signal_first_info.prefectures_code_ = ubyte_read(buf);
				ushort_data = ushort_read(buf);
				signal_first_info.offer_point_code_ = ushort_data >> 15;
				signal_first_info.intersection_id_ = ushort_data & 0x7F;
				//signal_first_info.intersection_id_ = (uint16_t)124;
				//std::cout << "p," << +signal_first_info.prefectures_code_ << std::endl;
				//std::cout << "o," << +signal_first_info.offer_point_code_ << std::endl;
				//std::cout << "f," << signal_first_info.intersection_id_ << std::endl;
				signal_first_info.specific_control_flag_ = ubyte_read(buf);
				signal_first_info.system_state_ = ubyte_read(buf);
				signal_first_info.event_counter_ = ubyte_read(buf);
				signal_first_info.car_lamp_count_ = ubyte_read(buf);
				signal_first_info.walk_lamp_count_ = ubyte_read(buf);
				signal_first_info.connection_routes_count_ = ubyte_read(buf);
				signal_first_info.service_routes_count_ = ubyte_read(buf);
				//std::cout << +signal_first_info.connection_routes_count_ << "," << +signal_first_info.service_routes_count_ << std::endl;
				ret.first_info = signal_first_info;

				for(int sind=0; sind<signal_first_info.service_routes_count_; sind++)
				{
					//ServiceInfo info;
					autoware_msgs::SipSignalServiceInfo info;
					info.routes_id_ = ubyte_read(buf);
					//if(sind == 0) info.routes_id_ = 3;
					info.direction_existence_flag_ = ubyte_read(buf) >> 15;
					info.direction_info_ = byte_read((char*)buf);
					//std::cout << +info.routes_id_ << "," << +info.direction_existence_flag_ << "," << +info.direction_info_ << std::endl;
					for(int i=0; i<signal_first_info.connection_routes_count_; i++)
						info.car_lamp_info_ptr_.push_back(short_read(buf));
					for(int i=0; i<signal_first_info.connection_routes_count_; i++)
						info.walk_lamp_info_ptr_.push_back(short_read(buf));
					/*std::cout << "car" << std::endl;
					for(int i=0; i<signal_first_info.connection_routes_count_; i++)
						std::cout << info.car_lamp_info_ptr_[i] << std::endl;
					std::cout << "walk" << std::endl;
					for(int i=0; i<signal_first_info.connection_routes_count_; i++)
						std::cout << info.walk_lamp_info_ptr_[i] << std::endl;*/
					ret.service_info.push_back(info);
				}

				for(int cind=0; cind<signal_first_info.car_lamp_count_; cind++)
				{
					//CarLampAllInfo info;
					autoware_msgs::SipSignalCarLampInfoList info;
					info.addres_number_ = read_address_ + 1;
					ubyte_data = ubyte_read(buf);
					info.car_lamp_id_ = ubyte_data >> 4;
					unsigned char color_change_count_ = ubyte_data & 0x0F;
					//std::cout << info.addres_number_ << "," << +info.car_lamp_id_ << "," << +color_change_count_ << std::endl;
					for(unsigned char cou=0; cou<color_change_count_; cou++)
					{
						//CarLampInfo car_info;
						autoware_msgs::SipSignalCarLampInfo car_info;
						car_info.round_signal_color_view_ = ubyte_read(buf);
						car_info.blue_arrow_signal_color_view_ = ubyte_read(buf);
						ushort_data = ushort_read(buf);
						car_info.countdown_stop_flag_ = ushort_data >> 15;
						car_info.min_remain_sec_ = ushort_data & 0x7FFF;
						car_info.max_remain_sec_ = ushort_read(buf);
						info.car_lamp_info_.push_back(car_info);
						if(cind == 0) std::cout << +car_info.round_signal_color_view_ << std::endl;
						//std::cout << +car_info.blue_arrow_signal_color_view_ << std::endl;
						//std::cout << +car_info.countdown_stop_flag_ << std::endl;
						//std::cout << car_info.min_remain_sec_ << std::endl;
						//std::cout << car_info.max_remain_sec_ << std::endl;
					}
					if(cind == 0) std::cout << std::endl;
					ret.car_lamp_info_list.push_back(info);
				}

				ret.header.stamp = ros::Time::now();
				pub_signal_info_.publish(ret);
			}

			
			//std::cout << "read_size," << read_address_ << std::endl;
			break;	
		}
	}
};

int main(int argc, char** argv)
{
	ros::init(argc,argv,"sip_car_controll_tcp");
	ros::NodeHandle nh, private_nh("~");

	SipCarControll controll(nh, private_nh);
	
	while(ros::ok())
	{
		controll.run();
	}
	return 0;
}